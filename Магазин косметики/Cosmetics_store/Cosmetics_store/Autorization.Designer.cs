﻿namespace Cosmetics_store
{
    partial class Autorization
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Autorization));
            this.padlock = new System.Windows.Forms.PictureBox();
            this.user = new System.Windows.Forms.PictureBox();
            this.authorize = new System.Windows.Forms.Button();
            this.password = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.TextBox();
            this.logo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.padlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // padlock
            // 
            this.padlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.padlock.Image = ((System.Drawing.Image)(resources.GetObject("padlock.Image")));
            this.padlock.InitialImage = null;
            this.padlock.Location = new System.Drawing.Point(50, 229);
            this.padlock.Name = "padlock";
            this.padlock.Size = new System.Drawing.Size(42, 40);
            this.padlock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.padlock.TabIndex = 24;
            this.padlock.TabStop = false;
            // 
            // user
            // 
            this.user.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.user.Image = ((System.Drawing.Image)(resources.GetObject("user.Image")));
            this.user.InitialImage = null;
            this.user.Location = new System.Drawing.Point(50, 141);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(42, 40);
            this.user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.user.TabIndex = 23;
            this.user.TabStop = false;
            // 
            // authorize
            // 
            this.authorize.BackColor = System.Drawing.Color.MistyRose;
            this.authorize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.authorize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.authorize.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authorize.ForeColor = System.Drawing.SystemColors.ControlText;
            this.authorize.Location = new System.Drawing.Point(0, 357);
            this.authorize.Name = "authorize";
            this.authorize.Size = new System.Drawing.Size(424, 44);
            this.authorize.TabIndex = 22;
            this.authorize.Text = "Войти";
            this.authorize.UseVisualStyleBackColor = false;
            this.authorize.Click += new System.EventHandler(this.button1_Click);
            // 
            // password
            // 
            this.password.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.password.Font = new System.Drawing.Font("Lucida Bright", 12F);
            this.password.Location = new System.Drawing.Point(139, 229);
            this.password.Multiline = true;
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(196, 40);
            this.password.TabIndex = 21;
            // 
            // login
            // 
            this.login.Font = new System.Drawing.Font("Lucida Bright", 12F);
            this.login.Location = new System.Drawing.Point(139, 141);
            this.login.Multiline = true;
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(196, 40);
            this.login.TabIndex = 20;
            // 
            // logo
            // 
            this.logo.BackColor = System.Drawing.Color.MistyRose;
            this.logo.Dock = System.Windows.Forms.DockStyle.Top;
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.Location = new System.Drawing.Point(0, 0);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(424, 135);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logo.TabIndex = 25;
            this.logo.TabStop = false;
            // 
            // Autorization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(424, 401);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.padlock);
            this.Controls.Add(this.user);
            this.Controls.Add(this.authorize);
            this.Controls.Add(this.password);
            this.Controls.Add(this.login);
            this.Font = new System.Drawing.Font("Palatino Linotype", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Autorization";
            this.Text = "Autorization";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Autorization_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.padlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox padlock;
        private System.Windows.Forms.PictureBox user;
        private System.Windows.Forms.Button authorize;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox login;
        private System.Windows.Forms.PictureBox logo;
    }
}

