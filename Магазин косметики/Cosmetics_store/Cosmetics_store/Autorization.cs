﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Cosmetics_store
{
    public partial class Autorization : Form
    {
        SqlConnection connection;
        SqlDataAdapter adapter;
        DataSet DataSet;
        SqlCommand comm;
        DataBaseSelect DataBase = new DataBaseSelect();

        Admin admin = new Admin();
        Seller seller = new Seller();
        Guest guest = new Guest();
        public Autorization()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                bool Open = false;
                using (connection = new SqlConnection(DataBase.ConnectDataBase()))
                {
                    adapter = new SqlDataAdapter("SELECT Логин, Пароль, ID_должности, ФИО_сотрудника FROM Сотрудники", connection);
                    DataSet = new DataSet();
                    adapter.Fill(DataSet);
                    for (int i = 0; i < DataSet.Tables[0].Rows.Count; i++)
                    {
                        if (login.Text == DataSet.Tables[0].Rows[i][0].ToString())
                        {
                            if (password.Text == DataSet.Tables[0].Rows[i][1].ToString())
                            {
                                switch (DataSet.Tables[0].Rows[i][2].ToString())
                                {
                                    case "2": admin.Show(); break;
                                    case "3": seller.Show(); break;
                                }
                                this.Hide();
                                Open = true;
                                break;
                            }
                        }
                    }
                }
                if (Open == false)
                {
                    MessageBox.Show("Неверный логин или пароль!");
                }
            }
            catch
            {
                MessageBox.Show("Возникли временные проблемы");
            }
        }

        private void Autorization_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
